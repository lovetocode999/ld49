--[[

Copyright (c) 2021 Elijah Gregg

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

--]]

-- Libraries
-- Gamestate support
Gamestate = require("libraries/hump/gamestate")
Class = require("libraries/hump/class")
Lighter = require("libraries/lighter")

-- Init main gamestate
local game = {}

-- Init outro gamestate
local outro = {}

-- Enter the outro
spaceKeyScene = nil
function outro:enter()
	UI:clear()
	UI:enter()
	font1 = love.graphics.newFont("assets/GemunuLibre-SemiBold.ttf", 27)
	font2 = love.graphics.newFont("assets/GemunuLibre-SemiBold.ttf", 50)
	font3 = love.graphics.newFont("assets/Roboto-Italic.ttf", 20)
	gameover = Gameover(font1, font2, font3, 225, 210, 350)
	UI:add(gameover)
end

-- Outro update function
function outro:update(dt)
	UI:update(dt)
	if love.keyboard.isDown("space") and spaceKeyScene == self then
		Gamestate.switch(game)
	end
end

-- Outro draw function
function outro:draw()
	UI:draw()
	love.graphics.setColor(1, 1, 1)
end

-- Game over screen
Gameover = Class{}

-- Init game over screen
function Gameover:init(font1, font2, font3, x, y, w)
	self.x = x
	self.y = y
	self.w = w
	self.font1 = font1
	self.font2 = font2
	self.font3 = font3
end

-- Update game over screen
function Gameover:update(dt)
end

-- Draw the game over screen
function Gameover:draw()
	love.graphics.setColor(1, 1, 1)
	love.graphics.setFont(self.font2)
	text = "Game Over"
	love.graphics.printf(text, self.x, self.y, self.w, "center")
	love.graphics.setFont(self.font1)
	text = "You died!\n"..
	       "Score: "..score.counter
	love.graphics.printf(text, self.x, self.y+55, self.w, "center")
	love.graphics.setFont(self.font3)
	text = "[press space to restart]"
	love.graphics.printf(text, self.x, self.y+120, self.w, "center")
end

-- Init intro gamestate
local intro = {}
intro.starting = 0

-- Enter the intro
function intro:enter()
	UI:clear()
	UI:enter()
	font1 = love.graphics.newFont("assets/GemunuLibre-SemiBold.ttf", 27)
	font2 = love.graphics.newFont("assets/GemunuLibre-SemiBold.ttf", 50)
	font3 = love.graphics.newFont("assets/Roboto-Italic.ttf", 20)
	welcome = Welcome(font1, font2, font3, 225, 210, 350)
	UI:add(welcome)
end

-- Intro update function
function intro:update(dt)
	UI:update(dt)
	if love.keyboard.isDown("space") and self.starting == 0 then
		self.starting = self.starting+dt
		love.audio.play(startSound)
	end
	if self.starting > 0 then
		self.starting = self.starting+dt
	end
	if self.starting > 2 then
		Gamestate.switch(game)
	end
end

-- Intro draw function
function intro:draw()
	UI:draw()
	love.graphics.setColor(1, 1, 1)
end

-- Welcome screen
Welcome = Class{}

-- Init welcome screen
function Welcome:init(font1, font2, font3, x, y, w)
	self.x = x
	self.y = y
	self.w = w
	self.timer = 0 
	self.frame = 0
	self.font1 = font1
	self.font2 = font2
	self.font3 = font3
end

-- Update welcome screen
function Welcome:update(dt)
	self.timer = self.timer + dt
	self.frame = math.ceil(self.timer*20)
end

-- Draw the welcome screen
function Welcome:draw()
	love.graphics.setColor(1-(intro.starting/2), 1-(intro.starting/2), 1-(intro.starting/2))
	love.graphics.setFont(self.font2)
	text = "A Solar Disorder"
	love.graphics.printf(text, self.x, self.y, self.w, "center")
	love.graphics.setFont(self.font1)
	text = "Mission: destroy the asteroids\n"..
         "Rate of fatality: 100%\n"..
         "Duration: however long you survive"
	love.graphics.printf(text, self.x, self.y+55, self.w, "left")
	love.graphics.setFont(self.font3)
	text = "[press space to begin]"
	love.graphics.printf(text, self.x, self.y+150, self.w, "center")
end

-- Initialize lighter
lighter = Lighter()
lightX, lightY = 400, 300
light = lighter:addLight(lightX, lightY, 160, 0.25, 0.25, 0.225)

-- Function that is run when the 'game' gamestate is started
score = {}
player = {}
sun = {}
boomSound = love.audio.newSource("assets/boom.wav", "static")
startSound = love.audio.newSource("assets/start.wav", "static")
fireSound = love.audio.newSource("assets/fire.wav", "static")
music = love.audio.newSource("assets/spacetune.wav", "static")
gameOverSound = love.audio.newSource("assets/gameover.wav", "static")
function game:enter()
	UI:clear()
	UI:enter()
	Entities:clear()
	Entities:enter()
	player = Player(world, 100, 100, 10, 25)
	player.fixture:setCategory(2)
	sun = Sun(world, 400, 300, 20, 60000000)
	score = Score(love.graphics.newFont(27, "normal", love.graphics.getDPIScale()), 5, 5, 100)
	health = Health(675, 5)
	ammo = Ammo(660, 568)
	Entities:add(player)
	Entities:add(sun)
	UI:add(score)
	UI:add(health)
	UI:add(ammo)
end

-- Game update function 
function game:update(dt)
	if not player.exists then
		love.audio.stop(fireSound)
		love.audio.play(gameOverSound)
		Gamestate.switch(outro)
	end
	Entities:update(dt)
	UI:update(dt)
	lighter:updateLight(light, lightX, lightY, sun.r*8, 0.25, 0.25-((sun.r - 20)*0.0025), 0.225-((sun.r - 20)*0.0025))
end

-- Game draw function
function game:draw()
	Entities:draw()
	lighter:drawLights()
	UI:draw()
	love.graphics.setColor(1, 1, 1)
end

-- Base entity object
local Entity = Class{}

-- Keep track of coming explosions
Explosions = {}

-- Create physics world
love.physics.setMeter(1) --1 pixel per meter
world = love.physics.newWorld(0, 0, true)

function beginContact(a, b, coll)
	local isFalseCollision = false
	if a:getUserData() == "bullet" and b:getUserData() == "player" then
		isFalseCollision = true
	elseif b:getUserData() == "bullet" and a:getUserData() == "player" then
		isFalseCollision = true
	elseif a:getUserData() == "bullet" and b:getUserData() == "bullet" then
		isFalseCollision = true
	end
	if not isFalseCollision then
		x = a:getBody():getX() + (b:getBody():getX() - a:getBody():getX())/2
		y = a:getBody():getY() + (b:getBody():getY() - a:getBody():getY())/2
		table.insert(Explosions, {x = x, y = y})
	end
	if a:getUserData() == "bullet" or b:getUserData() == "bullet" then
		if a:getUserData() == "sun" or b:getUserData() == "sun" then
			for i, e in ipairs(Entities.entityList) do
				if e.fixture:getUserData() ~= "sun" and (e.fixture == a or e.fixture == b) then
					bullets = bullets - 1
					table.remove(Entities.entityList, i)
					e.fixture:destroy()
					e.fixture:release()
					return
				end
			end
		elseif a:getUserData() == "asteroid" or b:getUserData() == "asteroid" then
			for i, e in ipairs(Entities.entityList) do
				if e.fixture:getUserData() ~= "asteroid" and (e.fixture == a or e.fixture == b) then 
					bullets = bullets - 1
					table.remove(Entities.entityList, i)
					e.fixture:destroy()
					e.fixture:release()
				elseif e.fixture:getUserData() == "asteroid" and (e.fixture == a or e.fixture == b) then
					asteroids = asteroids - 1
					table.remove(Entities.entityList, i)
					e.fixture:destroy()
					e.fixture:release()
					score.counter = score.counter + 1
				end
			end
			return
		end
	elseif a:getUserData() == "sun" or b:getUserData() == "sun" then
		for i, e in ipairs(Entities.entityList) do
			if e.fixture:getUserData() ~= "sun" and (e.fixture == a or e.fixture == b) then
				if e.fixture:getUserData() == "asteroid" then
					asteroids = asteroids - 1
				end
				if e.fixture:getUserData() == "player" then
					player.exists = false
				end
				table.remove(Entities.entityList, i)
				e.fixture:destroy()
				e.fixture:release()
				sun.r = sun.r+1
				sun.shape:setRadius(sun.r)
				sun.m = 3000000*sun.r
				sun.isNewFixture = true
				return
			end
		end
	elseif a:getUserData() == "player" or b:getUserData() == "player" then
		for i, e in ipairs(Entities.entityList) do
			if e.fixture:getUserData() ~= "player" and (e.fixture == a or e.fixture == b) then
				if e.fixture:getUserData() == "asteroid" then
					asteroids = asteroids - 1
				end
				table.remove(Entities.entityList, i)
				e.fixture:destroy()
				e.fixture:release()
				score.counter = score.counter + 1
				player.hp = player.hp - 1
				if player.hp < 0 then
					Entities:remove(player)
					player.exists = false
					player.fixture:destroy()
					player.fixture:release()
				end
				return
			end
		end
	else
		asteroids = asteroids - 2
		for i, e in ipairs(Entities.entityList) do
			if e.fixture == a then
				table.remove(Entities.entityList, i)
			end
		end	
		for i, e in ipairs(Entities.entityList) do
			if e.fixture == b then
				table.remove(Entities.entityList, i)
			end
		end	
		a:destroy()
		a:release()
		b:destroy()
		b:release()
	end
end

function endContact(a, b, coll)
end

function preSolve(a, b, coll)
end

function postSolve(a, b, coll, normalimpulse, tangentimpulse)
end
world:setCallbacks(beginContact, endContact, preSolve, postSolve)

-- Initialize the random number generator
math.randomseed(os.time())

-- Init entity (world, x position, y position, radius, mass)
function Entity:init(world, x, y, r, m)
	self.world = world
	self.x = x
	self.y = y
	self.r = r
	self.m = m
	self.body = love.physics.newBody(world, x, y, "dynamic")
	self.shape = love.physics.newCircleShape(r)
	self.fixture = love.physics.newFixture(self.body, self.shape)
end

-- Empty drawing function
function Entity:draw()
end

-- Empty update function
function Entity:update(dt)
end

-- UI: like Entities but for UI elements
UI = {
	active = true,
	entityList = {}
}

-- Enter UI
function UI:enter()
	self:clear()
end

-- Add new UI element
function UI:add(entity)
	table.insert(self.entityList, entity)
end

-- Delete UI element
function UI:remove(entity)
	for i, e in ipairs(self.entityList) do
		if e == entity then
			table.remove(self.entityList, i)
			return
		end
	end
end

-- Clear all UI elements
function UI:clear()
	self.entityList = {}
end

-- Draw all UI elements
function UI:draw()
	for i, e in ipairs(self.entityList) do
		e:draw()
	end
end

-- Update all UI elements (for menus, etc.)
function UI:update(dt)
	for i, e in ipairs(self.entityList) do
		e:update(dt)
	end
end

-- Init Entities object
Entities = {
	active = true,
	world = nil,
	entityList = {}
}

-- Enter entities
function Entities:enter(world)
	self:clear()
	self.world = world
end

-- Add new entity
function Entities:add(entity)
	table.insert(self.entityList, entity)
end

-- Delete entity
function Entities:remove(entity)
	for i, e in ipairs(self.entityList) do
		if e == entity then
			table.remove(self.entityList, i)
			return
		end
	end
end

-- Clear all entities
function Entities:clear()
	for i, e in ipairs(self.entityList) do
		e.fixture:destroy()
		e.fixture:release()
	end
	self.entityList = {}
	Explosions = {}
	score = {}
	player = {}
	sun = {}
	bullets = 0
	asteroids = 0
end

-- Draw all entities
function Entities:draw()
	for i, e in ipairs(self.entityList) do
		e:draw()
	end
end

-- Update all entities
function Entities:update(dt)
	-- Update physics
	world:update(dt)
	-- Run each object's update function
	for i,e in ipairs(self.entityList) do
		e:update(dt)
	end
	-- Update gravitational attraction between objects
	for i,e in ipairs(self.entityList) do
		if e.fixture:getUserData() ~= "bullet" then
			for j,f in ipairs(self.entityList) do
				if e ~= f then
					cx = e.body:getX()-f.body:getX()
					cy = e.body:getY()-f.body:getY()
					radius = math.sqrt(cx^2+cy^2)
					m1 = e.m
					m2 = f.m
					force = (m1*m2)/radius^2
					e.body:applyForce((-1*force*cx)/radius, (-1*force*cy)/radius)
				end
			end
		end
	end
	-- Create new asteroid
	if asteroids < 6 then
		if math.random(0, 1) == 1 then
			table.insert(self.entityList, Asteroid(world, math.random(0, 900)-50, (math.random(0, 1)*700)-50, 10, 25, love.graphics.newImage("assets/asteroid"..math.random(1,2)..".png")))
		else
			table.insert(self.entityList, Asteroid(world, (math.random(0, 1)*1000)-100, math.random(0, 800)-100, 10, 25, love.graphics.newImage("assets/asteroid"..math.random(1,2)..".png")))
		end
		asteroids = asteroids+1
	end
	-- Create necessary explosions
	for i, e in ipairs(Explosions) do
		explosion = Explosion(world, e.x, e.y)
		self:add(explosion)
		love.audio.stop(boomSound)
		boomSound:setPitch(math.random(0.9, 1.1))
		love.audio.play(boomSound)
	end
	Explosions = {}
end

-- Create the sun class
Sun = Class{
	_includes = Entity
}

-- Init the sun
function Sun:init(world, x, y, r, m)
	Entity.init(self, world, x, y, r, m)
	self.body:setType("static")
	self.fixture:setUserData("sun")
end

-- Update the sun 
function Sun:update(dt)
	if self.isNewFixture then
		self.fixture:destroy()
		self.fixture:release()
		self.fixture = love.physics.newFixture(self.body, self.shape)
		self.fixture:setUserData("sun")
		self.isNewFixture = false
	end
end

-- Draw the sun
function Sun:draw(dt)
	love.graphics.setColor(1, 1-((self.r - 20)*0.02), 0.9-((self.r - 20)*0.02))
	love.graphics.circle("fill", self.body:getX(), self.body:getY(), self.shape:getRadius())
end

-- Create the player class
Player = Class{
	_includes = Entity
}

-- Init the player
function Player:init(world, x, y, r, m)
	Entity.init(self, world, x, y, r, m)
	self.direction = 0
	self.fixture:setUserData("player")
	self.hp = 4
	self.ammo = 15
	self.timer = 0
	self.bulletTimer = 0
	self.frame = 0
	self.animationTimer = 0
	self.exists = true
end

-- Update the player
function Player:update(dt)
	if love.keyboard.isDown("up") or love.keyboard.isDown("w") then
		self.body:applyForce(math.cos((math.pi*self.direction)/180)*1000000, math.sin((math.pi*self.direction)/180)*1000000)
		local fire = Fire(self.world, self.body:getX()-self.shape:getRadius()*math.cos((math.pi*-self.direction)/180), self.body:getY()+self.shape:getRadius()*math.sin((math.pi*-self.direction)/180), 2, self.direction+180)
		Entities:add(fire)
		love.audio.play(fireSound)
	else
		love.audio.stop(fireSound)
	end
	if love.keyboard.isDown("left") or love.keyboard.isDown("a") then
		self.direction = self.direction - (90 * dt)
	elseif love.keyboard.isDown("right") or love.keyboard.isDown("d") then
		self.direction = self.direction + (90 * dt)
	end
	if love.keyboard.isDown("space") then
		if self.ammo > 0 and self.bulletTimer > 0.1 then
			local bullet = Bullet(self.world, self.body:getX()+self.shape:getRadius()*2*math.cos((math.pi*-self.direction)/180), self.body:getY()-self.shape:getRadius()*2*math.sin((math.pi*-self.direction)/180), 2, self.direction)
			Entities:add(bullet)
			bullets = bullets + 1
			bullet.body:applyForce(math.cos((math.pi*self.direction)/180)*1000000000, math.sin((math.pi*self.direction)/180)*1000000000)
			self.body:applyForce(math.cos((math.pi*self.direction)/180)*-1000000, math.sin((math.pi*self.direction)/180)*-1000000)
			self.ammo = self.ammo - 1
			self.bulletTimer = 0
			shootNoise = love.audio.newSource("assets/shoot.wav", "static")
			love.audio.play(shootNoise)
		end
	end
	self.bulletTimer = self.bulletTimer + dt
	self.timer = self.timer + dt
	if self.timer > 1 then
		self.ammo = self.ammo + 1
		if self.ammo > 15 then
			self.ammo = 15
		end
		self.timer = 0
	end
	if self.body:getX() > 800 then
		self.body:setX(800)
	elseif self.body:getX() < 0 then
		self.body:setX(0)
	end
	if self.body:getY() > 600 then
		self.body:setY(600)
	elseif self.body:getY() < 0 then
		self.body:setY(0)
	end
	self.animationTimer = self.animationTimer + dt
	self.frame = math.ceil(self.timer * 4)
	if self.frame > 16 then
		self.animationTimer = 0
		self.frame = 1
	elseif self.frame < 1 then
		self.frame = 16
		self.animationTimer = 0
	end
end

-- Draw the player
function Player:draw()
	love.graphics.setColor(1, 1, 1)
	love.graphics.draw(love.graphics.newImage("assets/rocket"..self.frame..".png"), self.body:getX(), self.body:getY(), math.rad(self.direction+90), 1, 1, 5.5, 10)
end

-- Asteroid class
Asteroid = Class{
	_includes = Entity
}

-- Init the asteroids
asteroids = 0
function Asteroid:init(world, x, y, r, m, image)
	Entity.init(self, world, x, y, r, m)
	if x > 800 then
		self.direction = 105
	elseif x < 0 then
		self.direction = 285
	elseif y < 0 then
		self.direction = 15
	elseif y > 600 then
		self.direction = 195
	end
	self.body:applyForce(math.cos((math.pi*self.direction)/180)*100000000, math.sin((math.pi*self.direction)/180)*100000000)
	self.fixture:setUserData("asteroid")
	self.image = image
end

-- Update the asteroids
function Asteroid:update(dt)
	if self.x > 1000 or self.x < -200 or self.y > 800 or self.y < -200 then
		asteroids = asteroids - 1
		Entities:remove(self)
	end
	self.direction = self.direction + math.random(0, 1)
end

-- Draw the asteroids
function Asteroid:draw()
	love.graphics.setColor(0.5, 0.5, 0.5)
	--love.graphics.circle("line", self.body:getX(), self.body:getY(), self.shape:getRadius())
	love.graphics.draw(self.image, self.body:getX(), self.body:getY(), (self.direction*math.pi)/180, 1, 1, 10, 10)
end

-- Bullet class
bullets = 0
Bullet = Class{
	_includes = Entity
}

-- Init the bullets
function Bullet:init(world, x, y, r, direction)
	Entity.init(self, world, x, y, r, 5)
	self.fixture:setUserData("bullet")
	self.fixture:setMask(2)
	self.direction = direction
end

-- Update the bullets
function Bullet:update(dt)
	if self.x > 1000 or self.x < -200 or self.y > 800 or self.y < -200 then
		bullets = bullets - 1
		Entities:remove(self)
	end
	self.body:applyForce(math.cos((math.pi*self.direction)/180)*1000000000, math.sin((math.pi*self.direction)/180)*1000000000)
end

-- Draw the bullets
function Bullet:draw()
	love.graphics.setColor(0, 0, 1)
	love.graphics.circle("fill", self.body:getX(), self.body:getY(), self.shape:getRadius())
end

-- Bullet class
Fire = Class{
	_includes = Entity
}

-- Init the fire
function Fire:init(world, x, y, r, direction)
	Entity.init(self, world, x, y, r, 5)
	self.fixture:setUserData("fire")
	self.fixture:setMask(1,2)
	self.direction = direction
end

-- Update the bullets
function Fire:update(dt)
	if self.x > 1000 or self.x < -200 or self.y > 800 or self.y < -200 then
		Entities:remove(self)
	end
	self.body:applyForce(math.cos((math.pi*self.direction)/180)*1000000000, math.sin((math.pi*self.direction)/180)*1000000000)
	if player.exists then
		distance, x1, y1, x2, y2 = love.physics.getDistance( self.fixture, player.fixture )
		if distance > 20 then
			Entities:remove(self)
		end
	else
		Entities:remove(self)
	end
end

-- Draw the bullets
function Fire:draw()
	if player.exists then
		distance, x1, y1, x2, y2 = love.physics.getDistance( self.fixture, player.fixture )
		love.graphics.setColor(1, 0 + (distance/20 * 0.5), 0)
		love.graphics.circle("fill", self.body:getX(), self.body:getY(), self.shape:getRadius())
	else
		Entities:remove(self)
	end
end

-- Score
Score = Class{}

-- Init the score
function Score:init(font, x, y, w)
	self.font = font
	self.x = x
	self.y = y
	self.w = w
	self.counter = 0
end

-- Empty update function (the score gets updated by the player object)
function Score:update(dt)
end

-- Score draw function
function Score:draw()
	love.graphics.setColor(1, 1, 1)
	love.graphics.setFont(self.font)
	love.graphics.printf(self.counter, self.x, self.y, self.w, "left")
end

-- HP
Health = Class{}

-- Init HP
function Health:init(x, y)
	self.x = x
	self.y = y
end

-- Empty update function (HP gets updated by the player object)
function Health:update(dt)
end

-- HP draw function
function Health:draw()
	love.graphics.setColor(1, 1, 1)
	if player.hp >= 0 then
		love.graphics.draw(love.graphics.newImage("assets/hp"..player.hp..".png"), self.x, self.y)
	end
end

-- Ammo
Ammo = Class{}

-- Init ammo
function Ammo:init(x, y)
	self.x = x
	self.y = y
end

-- Empty update function (ammo gets updated by the player object)
function Ammo:update(dt)
end

-- Ammo draw function
function Ammo:draw()
	love.graphics.setColor(1, 1, 1)
	if player.ammo >= 0 then
		love.graphics.draw(love.graphics.newImage("assets/ammo"..player.ammo..".png"), self.x, self.y)
	end
end

-- Explosion effect
Explosion = Class{}

-- Init explosion
function Explosion:init(world, x, y)
	Entity.init(self, world, x, y, 30, 0)
	self.fixture:setUserData("explosion")
	self.fixture:setMask(1,2)
	self.timer = 0
	self.frame = 1
end

-- Update the explosion
function Explosion:update(dt)
	self.timer = self.timer + dt
	self.frame = 14 - math.ceil(self.timer * 24.5)
	if self.frame < 1 then
		Entities:remove(self)
	end
end

-- Draw the explosion
function Explosion:draw()
	love.graphics.setColor(1, 1, 1)
	love.graphics.draw(love.graphics.newImage("assets/boom"..self.frame..".png"), self.body:getX(), self.body:getY(), 0, 1, 1, 30, 30)
end

-- Change what scene the space key was last pressed on
function love.keypressed(key, scancode, isRepeat)
	if key == "space" then
		spaceKeyScene = Gamestate.current()
	end
end

-- Loading function
function love.load()
  Gamestate.registerEvents()
  Gamestate.switch(intro)
end

-- vim:shiftwidth=2
